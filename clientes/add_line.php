<?php
	session_start();
	
    include("conn.php");
    //$mdb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
   
    $date = date('Y-m-d h:i:s');
    
    $idEnvio = $_REQUEST['idEnvio'];
    if ($idEnvio == 0){
        $sql = "INSERT INTO `envios` (`id`) VALUES (NULL)";
        $result = $mdb->prepare($sql);
        $result->execute();
        $_REQUEST['idEnvio'] = $mdb->lastInsertId();
    }
    
    $data = array_to_mysqli($_REQUEST); 
    if ($_POST['guardar'] ?? ''){
        
        $datos = [
            ":destinatario" => $_REQUEST['destinatario'] , 
            ":direccion" => $_REQUEST['direccion'], 
            ":cp" => $_REQUEST['cp'], 
            ":poblacion" => $_REQUEST['poblacion'], 
            ":telefono" => $_REQUEST['telefono']
        ];
        $sql = "INSERT INTO destinatarios (usuario_id, dest_name, dest_direccion, dest_poblacion, dest_codigop, dest_telefono) 
        VALUES ($_SESSION[cod], :destinatario, :direccion, :poblacion, :cp, :telefono)";
        

        $result = $mdb->prepare($sql);
        $result->execute($datos);
    } 

       
    unset($data[':guardar']);
    $sql = "INSERT INTO lineas(cliente, idFormulario, idEnvio, albaran, fecha, destinatario, direccion, cp, poblacion, telefono, horario, bultos, kilos, aplazada, observaciones, reembolso, portes, articulo, unidades, status, created_at, updated_at)
    VALUES ($_SESSION[cod], :idFormulario, :idEnvio, :albaran, :fecha, :destinatario, :direccion, :cp, :poblacion, :telefono,
    :horario, :bultos, :kilos, :aplazada, :observaciones, :reembolso, :portes, :articulo, :unidades, 
    0, :created_at, :updated_at)";
    

    $result = $mdb->prepare($sql);
    $result->execute($data);
    
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);
    $id = $mdb->lastInsertId();
    
    $sql = "SELECT * FROM lineas where id = '$id'";
    $result = $mdb->prepare($sql);
    $result->execute($data);

    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    echo $result;

