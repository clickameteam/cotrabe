<?php 
session_start();
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Panel Cotrabe</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" action="index.php">
      <img class="mb-4" src="cotrabe-baixa.png" alt="" width="300"> 
      <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email" required="" autofocus="">     
      <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required="">      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
       <?php 
      if ($_REQUEST['error']==1){
        echo "<br><span style='color:red'>Ha cerrado sesión</span>";
        $_SESSION['user']="";
        $_SESSION['nivel']="";
        $_SESSION['cod']="";
      }
      if ($_REQUEST['error']==2){
        echo "<br><span style='color:red'>Error de usuario o password</span>";
        $_SESSION['user']="";
        $_SESSION['nivel']="";
        $_SESSION['cod']="";
      }
      if ($_REQUEST['error']==3){
        echo "<br><span style='color:red'>Inicie sesión para ver el contenido</span>";
        $_SESSION['user']="";
        $_SESSION['nivel']="";
        $_SESSION['cod']="";

      }
    ?>
      <p class="mt-4 mb-3 text-muted">© <?php echo date('Y')?></p>
    </form>
   
  

</body></html>