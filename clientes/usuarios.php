<?php 
include("header.php");
$_SESSION['menu']=4;
$sqluser="SELECT * FROM usuarios WHERE nivel = 2";
$consultauser=mysqli_query($conexion,$sqluser);
$totaluser=mysqli_num_rows($consultauser);

if($_SESSION['user']==""){
  header('Location:sign-in.php?&error=3');
}

?>

  <body>

    <?php include("main.php")?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
            <h1 class="h2">Usuarios</h1>  <a href="newuser.php"> <button class="btn btn-primary btn-sm"> Nuevo</button>    </a>    
          </div>         
          <?php
          if (isset($_REQUEST['succes']) && $_REQUEST['succes']==1){
            echo "<h3 style='color:green'>Usuario creado correctamente.</h3>";
          }
          if (isset($_REQUEST['succes']) && $_REQUEST['succes']==2){
            echo "<h3 style='color:red'>Usuario eliminado correctamente.</h3>";
          }
          ?>
          
          <div class="table-responsive">
            <?php if ($totaluser>0){

              ?>
            <table class="table table-striped table-sm" id="example2">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Nivel</th>   
                  <td></td>               
                </tr>
              </thead>
              <tbody>
                <?php
                 while($resultadouser=mysqli_fetch_assoc($consultauser)){
                  if ($resultadouser['nivel']==1){
                    $nivel="Administrador";
                  }
                  if ($resultadouser['nivel']==2){
                    $nivel="Cliente";
                  }
                  ?>
                <tr>
                  <td><?php echo $resultadouser['id']?></td>
                  <td><?php echo $resultadouser['cod']?></td>
                  <td><?php echo $resultadouser['nombre']?></td>
                  <td><?php echo $resultadouser['email']?></td>
                  <td><?php echo $nivel?></td>    
                  <td><a href="deleteuser.php?&id=<?php echo $resultadouser['id']?>"><button class="btn btn-primary btn-sm"> Borrar</button></a> </td>              
                </tr>    
                <?php
                 }?>            
              </tbody>
            </table>
            <?php 
            }?>
          </div>
        </main>
    <?php include("footer.php")?>

<script>
    $(document).ready(function() {
        $('#example2').dataTable( {
            "stateSave": false,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "order": [[ 1, "asc" ]],
            "language": {
                "lengthMenu": "Registros a mostrar _MENU_ por página",
                "zeroRecords": "No he encontrado nada lo siento",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin registros disponibles",
                "infoFiltered": "(filtro de _MAX_ registros)",
                "sSearch": "Buscar",
                "oPaginate": {
                    'sFirst':    'Primero',
                    'sPrevious': 'Anterior',
                    'sNext':     'Siguiente',
                    'sLast':     'Último'
                    }
                }
            } ); 
        });
    </script> 
<script>
  const insert_line = element => {
    let inputs = element.closest('form').querySelectorAll('input');
    let elements = {};
    inputs.forEach(e => elements[e.name] = e.value);
    let consulta = $.post("add_line.php", elements)
    .done(function(){
      let tr = JSON.parse(consulta.responseText)[0];
      insert_row(tr);
      $("input").val('');
    })
  }
  const insert_row = tr => {
    $("#tbody").append(`
    <tr>
      <td><a href="javascript:;" onclick="eliminar_row(this, ${tr.id})">Eliminar ${tr.id}</a></td>
      <td>${tr.albaran}</td>
      <td>${tr.fecha}</td>
      <td>${tr.destinatario}</td>
      <td>${tr.direccion}</td>
      <td>${tr.cp}</td>
      <td>${tr.poblacion}</td>
      <td>${tr.telefono}</td>
      <td>${tr.horario}</td>
      <td>${tr.bultos}</td>
      <td>${tr.kilos}</td>
      <td>${tr.aplazada}</td>
      <td>${tr.observaciones}</td>
      <td>${tr.reembolso}</td>
      <td>${tr.portes}</td>
      <td>${tr.articulo}</td>
      <td>${tr.unidades}</td>
      <td>${tr.status}</td>
      <td>${tr.created_at}</td>
      <td>${tr.updated_at}</td>
    </tr>
    `);
  }
  const eliminar_row = (element, id) => {
    let respuesta = confirm(`¿Desea eliminar la línea con id ${id}?`)
    if (respuesta){
      $.get(`delete_line.php?id=${id}`)
      .done(function(){
        $(element.closest('tr')).remove();
      })
    }
    else{
      
    }
  }
  const init_table = _ => {
    let cliente = document.querySelector("input[name='cliente']").value;
    let consulta = $.get(`lines.php?cliente=${cliente}`)
    .done(function(){
      let trs = JSON.parse(consulta.responseText);
      $.each(trs, function(){
        insert_row(this);
      })
    })
  }
  const cerrar_formulario = _ => {
    let cliente = document.querySelector("input[name='cliente']").value;
    let respuesta = confirm(`¿Desea cerrar este formulario?`)
    if (respuesta){
      let consulta = $.get(`generate_csv.php?cliente=${cliente}`)
      .done(function(){
        alert("CSV generado");
        $("#tbody").empty();
        openInNewTab("http://" + location.hostname + "/ficheros/pdf/" + consulta.responseText);
      })
      .fail(function(){
        alert(consulta.responseText);
      })
    }
    else{
      
    }
  }
  const openInNewTab = url => {
    var win = window.open(url, '_blank');
    win.focus();
  }
  $(document).ready(_ => {
    init_table();
  })
</script>