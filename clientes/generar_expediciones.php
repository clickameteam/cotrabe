<?php
	include("header.php");
	include("conn.php");
	
	$_SESSION['menu'] = 5;
	$sqluser          = "SELECT * FROM usuarios WHERE nivel = 2";
	$consultauser     = mysqli_query($conexion, $sqluser);
	$totaluser        = mysqli_num_rows($consultauser);
	
	if ($_SESSION['user'] == "") {
		header('Location:sign-in.php?&error=3');
	}
	if ($_REQUEST['codcli'] ?? '') {
		$_SESSION['cod'] = explode('-', $_REQUEST['codcli'])[0];
	}
	$idCliente = $_SESSION['cod'];
	$cliente = "SELECT * FROM codisclients WHERE codi='$_SESSION[cod]'";
	$result = $mdb->prepare($cliente);
	$result->execute();
	$cliente = $result->fetch(PDO::FETCH_ASSOC)['nom'];
	$fecha = date('Y-m-d h:i:s');
	$idFormulario = $_REQUEST['idFormulario'] ?? 0;
	if (!($idFormulario)){
		$data = [
			':cliente' => $_SESSION['cod'],
			':fecha' => $fecha
			
		];
		$sql = "INSERT INTO ficheros(cliente, created_at, updated_at, status) VALUES (:cliente, :fecha, :fecha, 0)";
        $result = $mdb->prepare($sql);
        $result->execute($data);
		$idFormulario = $mdb->lastInsertId();
		die("<script>location.href='generar_expediciones.php?idFormulario=$idFormulario'</script>");
	}
	else{
		$sql = "SELECT * FROM ficheros WHERE id=:idFormulario";
		$data = [':idFormulario' => $_REQUEST['idFormulario']];
		$result = $mdb->prepare($sql);
        $result->execute($data);
        $_SESSION['cod'] = $result->fetch(PDO::FETCH_ASSOC)['cliente'];
	}
	$data = [':id' => $idFormulario];
	$sql = "SELECT * FROM ficheros WHERE id=:id";
	$result = $mdb->prepare($sql);
	$result->execute($data);
	$status = $result->fetch(PDO::FETCH_ASSOC)['status'];
	

?>

<body>

<?php include("main.php") ?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<input type="hidden" id="codigoCliente" value="<?= $_SESSION['cod']; ?>"/>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
		<h1 class="h2">Expedición</h1>
		<?= $cliente;?>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="table table-responsive">
				<thead class="table-dark">
				<tr>
					<th>Albarán</th>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Dirección</th>
					<th>CP</th>
					<th>Población</th>
					<th>Etiquetas</th>
					<th>Eliminar</th>
				</tr>
				</thead>
				<tbody id="tbody">
				
				</tbody>
			</table>
		</div>
	</div>
	<?php if ($_SESSION['nivel'] == 1) {
		$sqlclientes      = "SELECT * FROM codisclients";
		$consultaclientes = mysqli_query($conexion, $sqlclientes);
		?>
		<div class="col-md-12 col-xs-12">
			<?= $cliente;?>
		</div>
		
		
		<br>
		
		<?php
		if ($_SESSION['cod'] > 0 && $_SESSION['nivel'] == 1) {
			$sqlclientes       = "SELECT * FROM codisclients WHERE codi=" . $_SESSION['cod'];
			$consultaclientes  = mysqli_query($conexion, $sqlclientes);
			$resultadoclientes = mysqli_fetch_assoc($consultaclientes);
			?>
			<h4>Cliente <?php echo $_SESSION['cod'] . ' ' . $resultadoclientes['nom'] ?></h4><br><br>
			<a href="#" target="_BLANK" id="pdf" class="btn btn-outline-secondary d-none">Ver y descargar PDF</a>
			<?php
		}
	} ?>
	
	<div class="table-responsive">
		<?php if ($totalex ?? '' > 0) { ?>
			
			<?php
		} ?>
	
	</div>
	<form id="form" action="" class="<?= $status == 1 ? 'd-none' : '';?>">
		<input
			type="number"
			value="<?= $idFormulario;?>"
			disabled
			class="d-none"
		>
		<input 
			type="hidden" 
			name="idEnvio" 
			id="idEnvio" 
		/>
		<input
			type="hidden"
			value="<?= $_SESSION['cod'];?>"
		>
		<input
				type="number"
				placeholder="Su número de albarán" pattern="[A-Za-z0-9]+" class="form-control" name="albaran" max=12
				required
				id="albaran"
		>
		<input
				type="number"
				placeholder="Fecha del pedido AAAAMMDD" class="form-control" name="fecha" max=8 required>
		<br>
		<input
			type="text"
			placeholder="Nombre comercial del destinatario"
			pattern="[A-Za-z0-9]+" 
			list="posibles_destinatarios"
			class="form-control ui-state-default" 
			name="destinatario" 
			id="destinatario" 
			max=35
			onkeypress="checkIfEnter(event);"
			onblur="cambiarDataDeFormulario(this);"
		>
		
		<script>
			const checkIfEnter = event => {
				const ENTER_KEY_CODE = 13;
				if (event.keyCode == ENTER_KEY_CODE){
					let input = event.target;
					cambiarDataDeFormulario(input);
				}
			}
		</script>
		<datalist id="posibles_destinatarios">
		</datalist>

		<input
				type="text"
				placeholder="Dirección de envío" pattern="[A-Za-z0-9]+" class="form-control" name="direccion" id="direccion" required>
		<input
				type="number"
				placeholder="Código postal del envío" class="form-control" name="cp" id="cp" max=5 required>
		<input
				type="text"
				placeholder="Población del envío" pattern="[A-Za-z0-9]+" class="form-control" name="poblacion" id="poblacion" required>
		<input
				type="text"
				placeholder="Teléfono del cliente" pattern="[A-Za-z0-9]+" class="form-control" name="telefono" id="telefono" required>
		
		
		<input 
			type="checkbox" 
			id="guardar" 
			name="guardar"
		/>
		<label> Guardar datos del Destinatario </label>
		<br>
		
		<br>
		<input
				type="tect"
				placeholder="Horario de entrega" pattern="[A-Za-z0-9]+" class="form-control" name="horario" max=25 required>
		<input
				type="number"
				placeholder="Número de bultos del envío. Únicamente si sólo efectuamos el transporte"
				class="form-control" name="bultos" max=15 required>
		<input
				type="number"
				placeholder="Num de kilos del envío. Únicamente si sólo efectuamos el transporte"
				class="form-control" name="kilos" max=10 required>
		<input
				type="number"
				placeholder="Fecha de entrega aplazada (AAAAMMDD)" class="form-control" name="aplazada" max=10 required>
		<input
				type="text"
				placeholder="Comentarios para la entrega"
				pattern="[A-Za-z0-9]+" class="form-control" name="observaciones" max=48 required>
		<input
				type="number"
				placeholder="Importe a cobrar a la descarga (999999.99)" class="form-control" name="reembolso" max=8 required>
		<input
				type="text"
				placeholder="Tipo portes siempre pagados por el remitente cliente" pattern="[A-Za-z0-9]+"
				class="form-control" name="portes" max=7 required>
		<input
				type="text"
				placeholder="Su código de producto" pattern="[A-Za-z0-9]+" class="form-control" name="articulo" max=15 required>
		<input
				type="number"
				placeholder="Unidades del producto" class="form-control" name="unidades" max=6 required>
		<button type="button" class="btn btn-primary" onclick="insert_line(this, true);">
			Nuevo Pedido
		</button>
		<button type="button" class="btn btn-secondary" onclick="insert_line(this, false);">
			Añadir Artículo
		</button>
		<button type="button" id="finalizar" class="btn btn-success" onclick="cerrar_formulario()">
			Finalizar
		</button>
		
	</form>
</main>

<div id="resultado"></div>
<?php include("footer.php"); ?>

<script>
	$(document).ready(function () {
		$('#example2').dataTable({
			"stateSave": false,
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
			"order": [[1, "asc"]],
			"language": {
				"lengthMenu": "Registros a mostrar _MENU_ por página",
				"zeroRecords": "No he encontrado nada lo siento",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"infoEmpty": "Sin registros disponibles",
				"infoFiltered": "(filtro de _MAX_ registros)",
				"sSearch": "Buscar",
				"oPaginate": {
					'sFirst': 'Primero',
					'sPrevious': 'Anterior',
					'sNext': 'Siguiente',
					'sLast': 'Último'
				}
			}
		});
	});
</script>
<script>
	const idFormulario = <?= $idFormulario;?>;
	const status = <?= $status; ?>;
	const cliente = <?= $_SESSION['cod'];?>;
	
	const insert_line = (element, limpiar) => {
		let inputs = element.closest('form').querySelectorAll('input');
		if ($('#albaran').val() == '' || $('#albaran').val() == '0' || parseFloat($('#albaran').val()) == 0){
			alert("Debe introducir un número de albarán");
			return false;
		}
		let elements = {};
		inputs.forEach(e => elements[e.name] = e.value);
		elements.guardar = document.getElementById("guardar").checked ? 1 : '';
		console.log(elements);
		let consulta = $.post(`add_line.php?idFormulario=${idFormulario}`, elements)
				.done(function () {

					let tr = JSON.parse(consulta.responseText)[0];
					$("#idEnvio").val(tr.idEnvio);
					insert_row(tr);
					if (limpiar){
						$.each($(inputs), function () {
							if (this.name !== 'cliente') {
								$(this).val('');
							}
						})
					}
					else{
						$("input[name='unidades']").val('');
						$("input[name='articulo']").val('');
					}
					cargarTodosLosDestinatarios();
				})
	}
	const insert_row = tr => {
		let enlace = '';
		let accion = '';
		if (!status){
			accion=`<a href="javascript:;" onclick="eliminar_row(this, ${tr.id})">Eliminar</a>`;
		}
		
		if (tr.bultos == 0){
			enlace = `Sólo logística`;
		}
		else{
			enlace = `
				<a target='_BLANK' href='generar_etiquetas.php?id=${tr.id}'>Generar</a>
			`;
		}
		$("#tbody").append(`
		    <tr>
		      <td>${tr.albaran}</td>
		      <td>${tr.fecha}</td>
		      <td>${cliente}</td>
		      <td>${tr.direccion}</td>
		      <td>${tr.cp}</td>
		      <td>${tr.poblacion}</td>
		      <td>${enlace}</td>
		      <td>${accion}</td>
		    </tr>
		`);
	}
	const eliminar_row = (element, id) => {
		let respuesta = confirm(`¿Desea eliminar la línea con id ${id}?`)
		if (respuesta) {
			$.get(`delete_line.php?id=${id}`)
					.done(function () {
						$(element.closest('tr')).remove();
					})
		} else {
		
		}
	}
	const init_table = _ => {
		$("#tbody").empty();
		let url = `lines.php?idFormulario=${idFormulario}`;
		let consulta = $.get(url)
				.done(function () {
					let trs = JSON.parse(consulta.responseText);
					$.each(trs, function () {
						insert_row(this);
					})
				})
	}
	const changePDFURL = url => {
		$("#pdf").attr('href', url);
		$("#pdf").removeClass('d-none');
		$("#form").addClass("d-none");
	}
	const changeEtiquetasURL = url => {
		$("#etiquetas").attr('href', url);
	}
	const disabledLimpiarTabla = bool => {
		document.getElementById("limpiarTabla").disabled = bool;
	}
	const limpiarTabla = _ => {
		disabledLimpiarTabla(true);
	}
	const cerrar_formulario = _ => {
		let cliente = document.querySelector("#codigoCliente").value;
		let respuesta = confirm(`¿Desea cerrar este formulario?`)
		if (respuesta) {
			let consulta = $.get(`generate_csv.php?cliente=${cliente}&idFormulario=${idFormulario}`)
					.done(function () {
						let pdf = "http://" + location.hostname + "/clientes/ficheros/pdf/" + consulta.responseText;
						changePDFURL(pdf);
						disabledLimpiarTabla(false);
						status = 1;
						init_table();
					})
					.fail(function () {
						alert(consulta.responseText);
					})
		} else {
		
		}
	}
	
	const openInNewTab = url => {
		var win = window.open(url, '_blank');
		win.focus();
	}
	const disabledButton = _ => {
		let elements = $("#tbody tr");
		if (elements.length == 0){
			$("#finalizar").attr('disabled', true);
		}
		else{
			$("#finalizar").attr('disabled', false);
		}
	}
	setInterval(e => disabledButton(), 300);
	$(document).ready(_ => {
		init_table();
	})
let posibles_destinatarios;
const cargarTodosLosDestinatarios = _ => {
	$.get("destinatarios.php")
	.done(function(data){
		$("#posibles_destinatarios").empty();
		posibles_destinatarios = JSON.parse(data);
		$.each(posibles_destinatarios, function(){
			$("#posibles_destinatarios").append(`<option value="${this.dest_name}"></option>`)
		})
	})
}
window.onload = _ => {
	cargarTodosLosDestinatarios();
}
const cambiarDataDeFormulario = element => {
	let dest_id = element.value;
	
	let dest = posibles_destinatarios.filter(destinatario => destinatario.dest_name == element.value)[0]; 
		
	$("#destinatario").val(dest.dest_name);
	$("#direccion").val(dest.dest_direccion);
	$("#cp").val(dest.dest_codigop);
	$("#poblacion").val(dest.dest_poblacion);
	$("#telefono").val(dest.dest_telefono);
	$("#guardar").attr("Disabled", true);
	
}



/*$(function () {

$("#destinatario").autocomplete({
	minLength: 0,
	source : function( request, response ) {
		$.ajax({
			url: "destinatarios.php",
			method: 'GET',
			dataType: "json",
			data: {
				name: request.term
			},
			success: function (data) {
				response( data );
			}
		});
	},
	focus: function (event, ui) {
		$("#destinatario").val(ui.item.dest_name);
		return false;
	},
	select: function (event, ui) {
		$("#destinatario").val(ui.item.dest_name);
		$("#direccion").val(ui.item.dest_direccion);
		$("#cp").val(ui.item.dest_codigop);
		$("#poblacion").val(ui.item.dest_poblacion);
		$("#telefono").val(ui.item.dest_telefono);
		                    

		return false;
	}
})
	.data("ui-autocomplete")._renderItem = function (ul, item) {
		return $("<li>")
			.data("ui-autocomplete-item", item)
			.append("<a> " + item.dest_name + "<br></a>")
			.appendTo(ul);
	};
});*/
</script>