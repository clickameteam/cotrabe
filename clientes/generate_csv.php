<?php
    error_reporting(E_ALL);
    error_reporting(-1);
    ini_set('error_reporting', E_ALL);
    session_start();

    include("conn.php");
    
    require_once("mpdf/vendor/autoload.php");
    require_once("clases/GenerateCSV.php");
    require_once("clases/GeneratePDF.php");
    require_once("clases/GenerateEtiquetas.php");
    $sql = "SELECT * FROM ficheros WHERE id=:idFormulario";
    $result = $mdb->prepare($sql);
    $data = ['idFormulario' => $_REQUEST['idFormulario']];
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);
    $result = $result[0];
    $fechaFormulario = $result['created_at'];
    
    $sql = "SELECT * FROM codisclients WHERE codi=:cliente";
    $result = $mdb->prepare($sql);
    $data = [':cliente' => $_REQUEST['cliente']];
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);
    $result = $result[0];
    $cliente = $result['nom'];
    $clientePDF = $result['codi'] . "&nbsp;&nbsp; $result[nom]";
    $fecha = date('d/m/Y h:i:s');
    $nowSQL = date('Y-m-d h:i:s');
    $destinatario = $result['nom'];
    $sql = "SELECT * FROM lineas WHERE status='0' AND cliente=:cliente AND idFormulario=:idFormulario";
    $result = $mdb->prepare($sql);
    $data = [':cliente' => $_REQUEST['cliente'],
        ':idFormulario' => $_REQUEST['idFormulario']
    ];
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);
    $timeFile = date('d.m.y_h.i.s');
    $nombreCSV = "$_REQUEST[cliente]_" . $timeFile .'.csv';
    $nombrePDF = "$_REQUEST[cliente]_" . $timeFile .'.pdf';
	$cliente = $_SESSION['cod'];
	$sql = "SELECT * FROM codisclients WHERE codi=:cliente";
	$data = [':cliente' => $cliente];
	$result2 = $mdb->prepare($sql);
	$result2->execute($data);
	$result2 = $result2->fetch(PDO::FETCH_ASSOC);
	$cliente_nom = $result2['nom'];
	
    $total = sizeof($result);
    if ($total == 0){
        header("HTTP/1.1 422 No hay órdenes que procesar");
        die('Error - No hay órdenes que procesar');
    }
    $csv = new GenerateCSV($nombreCSV, $result);
    $csv->generate();

    $pdf = new GeneratePDF($nombrePDF, $result, $clientePDF, $cliente_nom, $fechaFormulario);
    $pdf->generate();

    $etiquetas = new GenerateEtiquetas($nombrePDF, $result, $destinatario);
    $etiquetas->generate();

    $sql = "UPDATE ficheros set status='1', pdf='$nombrePDF', csv='$nombreCSV', updated_at='$nowSQL' WHERE status='0' AND cliente=:cliente AND id=:idFormulario";
    $result = $mdb->prepare($sql);
    $data = [':cliente' => $_REQUEST['cliente'],
        ':idFormulario' => $_REQUEST['idFormulario']
    ];
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);


    $sql = "UPDATE lineas set status='1', updated_at='$nowSQL' WHERE status='0' AND cliente=:cliente AND idFormulario=:idFormulario";
    $result = $mdb->prepare($sql);
    $data = [':cliente' => $_REQUEST['cliente'],
        ':idFormulario' => $_REQUEST['idFormulario']
    ];
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);
    echo $nombrePDF;