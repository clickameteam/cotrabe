<?php
    class GenerateCSV{

        private $name; //Name of CSV
        private $data; //Data passed to CSV
        
        public function __construct($name, $data){
            $this->name = $name;
            $this->data = $data;
            $this->csv = fopen("ficheros/csv/{$this->name}", "w");
        }
        public function generate(){
            $bultos = 0;
            $kgs = 0;
            foreach($this->data as $key => $value){
                $line = "$value[cliente];$value[albaran];$value[fecha];$value[destinatario];$value[direccion];$value[cp];$value[poblacion];$value[telefono];$value[horario];$value[bultos];$value[kilos];$value[aplazada];$value[observaciones];$value[reembolso];$value[portes];$value[articulo];$value[unidades];$value[idEnvio]\r\n";    
                fwrite($this->csv, $line);
            }
            fclose($this->csv);
        }
        function __toString(){

        }
    }