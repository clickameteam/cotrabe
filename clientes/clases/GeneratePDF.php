<?php
    class GeneratePDF{
        /**
         * Propiedades pasadas al constructor
         */
        private $name; //Name of pdf.
        private $data; //Data for construct the pdf.
        private $cliente; //Cómo se mostrará el cliente
        private $fechaFormulario; //Fecha de la creación del formulario
        /**
         * Propiedades propias de la clase
         */
        private $html;
        private $style;
        private $table;
        private $footer;
        private $header;

        public function __construct($name, $data, $cliente_completo, $cliente, $fechaFormulario){
            $this->name = $name;
            $this->data = $data;
            $this->cliente = $cliente;
            $this->fechaFormulario = $fechaFormulario;
            
        }

        public function generate(){
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'Legal'
            ]);
            $mpdf->AddPage('L');
            $this->setStyle();
            $this->setHeader();
            $this->setTable();
            $document = $this->style . $this->header . $this->table;
            $mpdf->WriteHTML($document);
            $mpdf->Output($this->name, 'F');
            rename($this->name, "ficheros/pdf/{$this->name}");

        }
        public function setStyle(){
            $this->style = "
                <style>
                    .headerPadre{
                        padding:0px;
                    }
                    .h1{
                        width:15%;
                        float:left;
                        display:inline-block;
                        text-align: right;
                    }
                    .h2{
                        width:40%;
                        float:left;
                        display:inline-block;
                        text-align: right;
                    }
                    .h3{
                        width:20%;
                        float:left;
                        display:inline-block;
                        text-align: right;

                    }
                    .h4{
                        text-align: right;
                        width:24%;
                        float:left;
                        display:inline-block;

                    }
                </style>
            ";
        }
        public function setHeader(){
            
            $fecha = date('d/m/Y h:i:s');
            
            $this->header = "
                <div class='headerPadre'>
                    <div class='h1'>
                        <img src='cotrabe-baixa.png'/>
                    </div>
                    <div class='h2'><h3>Listado de envíos para el Transportista</h3></div>
                    
                    <div class='h3'><b>Fecha:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {$fecha}</div>
                    <div class='h4'>{PAGENO}/{nb}</div>
                </div>
            " . 
            "<hr/>
                <div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cliente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$this->cliente}
                </div>
                <br>
                
            "
            ;
        }

        public function setTable(){
            $bultos = 0;
            $kgs = 0;
            $trs = '';
            $fecha = date('d/m/Y h:i:s');
            $total = 0;;
            foreach($this->data as $key => $value){
                $timestamp = strtotime($this->fechaFormulario);
                $fecha =  date("d/m/Y H:i", $timestamp);
                $trs .= "
                    <tr>
                        <td>{$fecha}</td>
                        <td>$value[albaran]</td>
                        <td>$value[idEnvio]</td>
                        <td>$value[destinatario]</td>
                        <td>$value[direccion]</td>
                        <td>$value[poblacion]</td>
                        <td>011</td>
                        <td>$value[cp]</td>
                        <td>P</td>
                        <td>$value[bultos]</td>
                        <td>$value[kilos]</td>
                        <td>$value[reembolso]</td>
                    </tr>
                ";
                $bultos += (float)$value['bultos'];
                $kgs += (float)$value['kilos'];
                $total++;
                
            }
            $trs .= "
                <tr>
                    <td colspan=12>&nbsp;</td>
                </tr>
                <tr  style='font-size:14px;'>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        <b>Total Envíos Cliente</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$total}
                    </td>
                    <td colspan=2 style='text-align:right;'>
                    </td>
                    <td colspan=2>
                        &nbsp;
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        Bultos: $bultos
                    </td>
                    <td>
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        Kgs: $kgs
                    </td>
                </tr>
                <tr>
                    <td colspan=12>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td colspan=12>&nbsp;</td>
                </tr>
                <tr  style='font-size:14px;'>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        <b>Total Envíos</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$total 
                    </td>
                    <td colspan=2 style='text-align:right;'>
                    </td>
                    <td colspan=2>
                        &nbsp;
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        Bultos: $bultos
                    </td>
                    <td>
                    </td>
                    <td colspan=2  style='font-size:14px;'>
                        Kgs: $kgs
                    </td>
                </tr>
            ";
            $table = "
                <table style='width:100%; font-size:10px;'>
                    <thead style='border:2px solid black;'>
                        <tr>
                            <th>Fecha</th>
                            <th>Referencia</th>
                            <th>Envío</th>
                            <th>Consignatario</th>
                            <th>Dirección</th>
                            <th>Destino</th>
                            <th>País</th>
                            <th>C.P.</th>
                            <th>P/D</th>
                            <th>Bultos</th>
                            <th>Kgs.</th>
                            <th>Reemb</th>
                        </tr>
                        <tr>
                            <th colspan=12><hr/></th>
                        </tr>
                    </thead>
                    <tbody>
                        $trs
                    </tbody>
                </table>
            ";
            $this->table = $table;
            
        }
        
    }