<?php

require 'vendor/autoload.php';

use Mpdf\Mpdf;
use Spipu\Html2Pdf\Html2Pdf;

class GenerateEtiquetas
{
    /**
     * Propiedades pasadas al constructor
     */
    private $name; //Name of pdf.
    private $data; //Data for construct the pdf.
    private $cliente; //Cómo se mostrará el cliente

    /**
     * Propiedades propias de la clase
     */
    private $html;
    private $style;
    private $table;
    private $footer;
    private $header;
    private $etiquetas = '';
    private $remitente = '';
    public function __construct($name, $data, $remitente)
    {
        $this->name = $name;
        $this->data = $data;
        $this->remitente = $remitente;
    }

    public function generate()
    {
        $mpdf = new Mpdf([
            'format' => 'Legal',
            'default_font_size' => 10,
	        'default_font' => 'Arial'
        ]);
        $img =  fopen('cotrabe-baixa.png', "r");
        $contenido = fread($img, filesize('cotrabe-baixa.png'));
        $mpdf->imageVars['image'] = $contenido;
        $mpdf->showImageErrors = true;
        $mpdf->AddPage('P', // L - landscape, P - portrait 
        '', '', '', '',
        1, // margin_left
        1, // margin right
        0, // margin top
        0, // margin bottom
        0, // margin header
        0); // margin footer);
        $this->setStyle();
        $this->setEtiquetas();
        $mpdf->defaultheaderline = 0;
        $mpdf->defaultfooterline = 0;
        $document = $this->style . $this->etiquetas . "</body></html>";
        $mpdf->WriteHTML($document);
        $mpdf->Output($this->name, 'F');
        rename($this->name, "ficheros/etiquetas/{$this->name}");
    }
    public function setStyle()
    {
        $this->style = "
                <html>
                    <head>
                        <meta charset='UTF-8'>
                        <style>
                            @page {
                                padding: 0px;
                                style='page-break-after:never';                             
                            }
                            .headerPadre{
                                margin: 0px;
                                padding:0px;
                            }
                            .h1{
                                width:15%;
                                float:left;
                                display:inline-block;
                                text-align: right;
                            }
                            .h2{
                                width:40%;
                                float:left;
                                display:inline-block;
                                text-align: right;
                            }
                            .h3{
                                width:20%;
                                float:left;
                                display:inline-block;
                                text-align: right;
        
                            }
                            .h4{
                                text-align: right;
                                width:24%;
                                float:left;
                                display:inline-block;
        
                            }
                            .w-100{
                                width: 100%;
                                clear: both;
                                padding: 0 10px 0 20px;
                            }
                            .w-50{
                                width: 50%;
                                padding: 0;
                            }
                            .w-75{
                                width: 73%;
                                display: inline;
                                padding: 0;
                            }
                            .w-25{
                                width: 23%;
                                display: inline;
                                padding: 0;
                            }
                            .d-inline{
                                float: left;
                                display:inline-block;
        
                            }
                            .inline{
                                display: inline;
                            }
                            .etiqueta{
                                margin-bottom: 0px;
                                vertical-align: text-top;
                                height: 445px!important;  
                                border:0px solid black;
                            }
                            .tabla-central{
                                padding: 0;
                                margin: 0 20px;
                                clear: both;
                                border:3px solid black;
                            }
                            .text-center{
                                text-align: center;
                            }
                            .cl{
                                clear: left;
                            }
                            .parEtiqueta{
                                margin-top: 1px;
                                border:3px solid black;
                            }
                            borde {
                                border:3px solid black;
                            }
                        </style>
                    </head>
                    <body>
            ";
    }
    public function generateBarcode($registro, $bultoN)
    {
        
        $ean = "3";
        $ean .= str_pad($bultoN, 3, "0", STR_PAD_LEFT);
        $ean .= str_pad($registro['idEnvio'], 8, "0", STR_PAD_LEFT);
      
        $ean = $ean . $this->controlDigit($ean);
        
        $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
        $barcode = $generator->getBarcode($ean, $generator::TYPE_EAN_13, 3, 45, [0,0,0]);
        return '<img src="data:image/jpg;base64,' . base64_encode($barcode) . '">';
        //die;
    }
    public function generateEan($registro, $bultoN)
    {
        
        $ean = "3";
        $ean .= str_pad($bultoN, 3, "0", STR_PAD_LEFT);
        $ean .= str_pad($registro['idEnvio'], 8, "0", STR_PAD_LEFT);
      
        $ean = $ean . $this->controlDigit($ean);
        
        
        return $ean;
        //die;
    }
    public function controlDigit($ean){
        $par=0;
        $impar=0;
        $first=1;
      
        // Empezamos por el final
        for ($i=strlen($ean)-1; $i>=0; $i--){
          if($first%2 == 0){
            $par += $ean[$i];
          }else{
            $impar += $ean[$i]*3;
          }
          $first++;
        }
        $control = ($par+$impar)%10;
        if($control > 0){$control = 10 - $control;}
        return $control;
    }
    public function generateEtiqueta($registro, $bultoN)
    {
        $barcode = $this->generateBarcode($registro, $bultoN);
        $fecha = date('d/m/Y');
        $ean = $this->generateEan($registro, $bultoN);

        $etiqueta = " 
                <div class='etiqueta w-50 d-inline'>
                    <div class='w-100 d-inline '>
                        <br/>
                    </div>
                    <div class='w-100 d-inline'style='text-align: right;'>
                        <img src='var:image' width='160px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class='w-100 d-inline '>
                        <br/>
                        Remitente:<b> {$this->remitente}</b>
                    </div>
                    
                    <br>
                    <div class='tabla-central' style='border:1px solid black;'>
                        <div class='w-100 ' style='padding-left: 10px;'>
                            <div class='w-100 d-inline'><b>
                                {$registro['destinatario']}<br/>
                                {$registro['direccion']}<br/>
                                {$registro['poblacion']}</b>
                            </div>
                        </div>                        
                    </div>
                    <br>
                    <div class='footer w-100'>                        
                        <div class='w-100'>
                            <div class='w-25 d-inline '>
                                Envio
                            </div>                            
                            <div class='w-25 d-inline'>
                                {$registro['idEnvio']}
                            </div>    
                            <div class='w-25 d-inline'>
                                Albarán
                            </div>    
                            <div class='w-25 d-inline'>
                                {$registro['albaran']}
                            </div>
                        </div>
                        <div class='w-100'>
                            <div class='w-25 d-inline'>
                                Portes
                            </div>    
                            <div class='w-25 d-inline'>
                                PAGADOS
                            </div>    
                            <div class='w-25 d-inline'>
                                Fecha
                            </div>    
                            <div class='w-25 d-inline'>
                                {$fecha}
                            </div>
                        </div>
                        <div class='w-100'>
                            <div class='w-25 d-inline'>
                                Bultos
                            </div>    
                            <div class='w-25 d-inline'>
                                {$bultoN}/{$registro['bultos']}
                            </div>    
                            <div class='w-25 d-inline'>
                                Kilos
                            </div>    
                            <div class='w-25 d-inline'>
                                {$registro['kilos']}
                            </div>
                        </div>
                        <div class='w-100'>
                            <div class='w-25 d-inline'>
                                Horario
                            </div>
                            <div class='w-75 d-inline'>
                                {$registro['horario']}
                            </div>
                        </div>
                        <br>
                        <div class='w-100 text-center'>
                            {$barcode}<br>
                            <span style='letter-spacing: 5px;'>{$ean}</span>
                        </div>
                        
                    </div>
                </div>";
        return $etiqueta;
    }
    public function setEtiquetas()
    {
        $etiquetas = '';
        $count = 0;
        foreach ($this->data as $key => $value) {
            for ($i = 0; $i < $value['bultos']; $i++) {
                $pre = '';
                $post = '';
                $count++;
                /*
                if ($count % 2 == 1) {
                    $pre .= "<div class='w-100 borde'>";
                } else {
                    $post .= "</div>";
                }*/                               
                /*
                if ($count == 4) {
                    $post .= "<div class='w-100' style='height: 28px; border: 0px solid black;'><br></div>";
                }
                */
                if ($count % 5 == 6) {
                    
                    $post .= '<div class="pagebreak"></div>';
                }
                $etiquetas .= ($pre . $this->generateEtiqueta($value, $i + 1) . $post);
            }
        }

        $this->etiquetas = $etiquetas;
    }
}
