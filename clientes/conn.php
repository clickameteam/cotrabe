<?php
	
	date_default_timezone_set('Europe/Andorra');
	global $db;
	global $mbd;
	/* prod */	
	$db         = new mysqli('localhost', 'cotrabe', 'cotrabe%', 'cotrabe');
	$host       = 'localhost';
	$dbname     = 'cotrabe';
	$usuario    = 'cotrabe';
	$contraseña = 'cotrabe%';
	/* dev */
	/*
	$db         = new mysqli('localhost', 'cotrabedev', '5Ju$qt40', 'cotrabedev');
	$host       = 'localhost';
	$dbname     = 'cotrabedev';
	$usuario    = 'cotrabedev';
	$contraseña = '5Ju$qt40';
	*/
	$mdb =
		new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $usuario, $contraseña) or die('No podemos conectar con la base de datos: [' . $db->connect_error . ']');
	if (!function_exists('array_to_mysqli')) {
		function array_to_mysqli($data)
		{
			$newData = [];
			foreach ($data as $key => $value) {
				if ($key == "password" || $key == "pass") {
					$newData[":$key"] = base64_encode($value);
				} else {
					$newData[":$key"] = $value;
				}
			}
			$ahora   = date('Y-m-d H:i:s');
			$newData = $newData + ['created_at' => $ahora, 'updated_at' => $ahora];
			return $newData;
		}
		
		;
	}
	if (!function_exists('fechaEuropea')) {
		function fechaEuropea($date)
		{
			$timestamp = strtotime($date);
			return date("d/m/Y H:i ", $timestamp);
		}
	}


?>