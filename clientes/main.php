<?php
    
    include('conn.php');
    
    $sqlnombre = "SELECT * FROM usuarios WHERE id=" . $_SESSION[ 'user' ];
    //echo $sqlnombre;
    $consulta = mysqli_query($conexion, $sqlnombre);
    $resultado = mysqli_fetch_assoc($consulta);
    if ($_SESSION[ 'cod' ] ?? '') {
        $data = [ ':cliente' => $_SESSION[ 'cod' ] ];
        $sql = "SELECT * FROM ficheros WHERE cliente=:cliente AND status=0 ORDER BY id DESC LIMIT 1";
        $result = $mdb->prepare($sql);
        $result->execute($data);
        $lastFichero = $result->fetch(PDO::FETCH_ASSOC)[ 'id' ] ?? 0;
    }
    
    $thefolder = "../../intranet/";
    
    if (is_dir($thefolder)) {
        if ($directorio = opendir($thefolder)) {
            while ($documento = readdir($directorio)) {
                $mystring = $documento;
                $findme = 'EXP_';
                $pos = strpos($mystring, $findme);
                
                if ($pos === 0) {
                    
                    //echo "Document : ".$documento."<br>";
                    $datafitxer = date("d-m-Y H:i:s", filemtime($thefolder . $documento));
                    //echo "Document : ".date("d-m-Y H:i:s",filemtime($thefolder.$documento))."<br>";
                }
            }
        }
    }

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
      integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
      crossorigin="anonymous"/>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="almacen.php">

        Panel Cotrabe - <?php
            echo $resultado[ 'nombre' ] ?></a>

    <ul class="navbar-nav px-3">

        <li class="nav-item text-nowrap">

            <a class="nav-link" href="sign-in.php?&error=1">Salir</a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <img src="cotrabe-baixa.png" width="300">
                <?php
                    if ($_SESSION[ 'nivel' ] == 1) { ?>
                        <br><span>Última actualización: <?= $datafitxer ?? ''; ?></span>
                    <?php
                    } ?>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <!--<a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 1) {
                                echo "active";
                            } ?>" href=".">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
              Inicio <span class="sr-only">(current)</span>
            </a>
          </li>-->
                    <li class="nav-item">
                        <a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 3) {
                                echo "active";
                            } ?>" href="almacen.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-bar-chart-2">
                                <line x1="18" y1="20" x2="18" y2="10"></line>
                                <line x1="12" y1="20" x2="12" y2="4"></line>
                                <line x1="6" y1="20" x2="6" y2="14"></line>
                            </svg>
                            Almacen
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 2) {
                                echo "active";
                            } ?>" href="expediciones.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-file">
                                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                <polyline points="13 2 13 9 20 9"></polyline>
                            </svg>
                            Expediciones
                        </a>
                    </li>
                    <?php
                        if ($_SESSION[ 'nivel' ] == 1) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?php
                                    if ($_SESSION[ 'menu' ] == 4) {
                                        echo "active";
                                    } ?>" href="usuarios.php">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-users">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                    Usuarios
                                </a>
                            </li>
                            
                            <?php
                        } ?>
                    <li class="nav-item ">
                        <a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 5) {
                                echo 'active';
                            } ?> <?php
                            if (!($_SESSION[ 'cod' ] ?? '')) {
                                echo 'd-none';
                            } ?>" href="generar_expediciones.php?idFormulario=<?= $lastFichero; ?>">
                            <i class="far fa-file-excel"></i>
                            Registrar expediciones
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 6) {
                                echo "active";
                            } ?>" href="listado_ficheros.php">
                            <i class="fas fa-list"></i>
                            Envío de archivo
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php
                            if ($_SESSION[ 'menu' ] == 7) {
                                echo "active";
                            } ?>" href="listado_destinatarios.php">
                            <i class="fas fa-list"></i>
                            Libreta destinatarios
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link <?php if ($_SESSION['menu'] == 8){echo "active";}?>" href="subir_csv.php">
                            <i class="fas fa-file-csv" style="font-size: 26px; "></i>
                            Etiquetas CSV
                        </a>
                    </li>
                </ul>
            </div>
        </nav>