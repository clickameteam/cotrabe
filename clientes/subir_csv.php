<?php
	
	include("header.php");
	include("conn.php");
	
	$_SESSION[ 'menu' ] = 8;
	if (!($_SESSION[ 'user' ] ?? FALSE)) {
		header('Location:sign-in.php?&error=3');
	}
	if (isset($_REQUEST[ 'codcli' ])) {
		$porciones = explode("-", $_REQUEST[ 'codcli' ]);
		$_SESSION[ 'cod' ] = $porciones[ 0 ];
		$_SESSION['nombre_cliente'] =  $porciones[1];
	}
	
	if ($_REQUEST[ 'idFormulario' ] ?? FALSE) {
		$sqlQueryParams = [ ':idFormulario' => $_REQUEST[ 'idFormulario' ] ];
		$sql = "SELECT * FROM lineas WHERE idFormulario = :idFormulario AND status <> 9";
		$result = $mdb->prepare($sql);
		$result->execute($sqlQueryParams);
		$lineas = $result->fetchAll(PDO::FETCH_ASSOC);
	}


?>
    <body>

<?php
include("main.php")
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="row mt-5">
			<?php
				if ($_SESSION[ 'nivel' ] == 1) {
					$sqlclientes = "SELECT * FROM codisclients";
					$consultaclientes = mysqli_query($conexion, $sqlclientes);
					?>
                    <form method="post" action="subir_csv.php">
                        <div class="col-12">
                            <div class="row">

                                <div class="col-md-6 col-xs-12">
                                    <input list="codcli" class="form-control" name="codcli" type="text"
                                           placeholder="Elige un cliente" required>
                                    <datalist id="codcli">
										<?php
											while ($resultadoclientes = mysqli_fetch_assoc($consultaclientes)){
										?>
                                        <option value="<?php
											echo $resultadoclientes[ 'codi' ] . '-' . $resultadoclientes[ 'nom' ] ?>">
											<?php
												} ?>

                                    </datalist>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <input type="submit" class="btn btn-primary btn-sm form-control"
                                           value="Escoger cliente">
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
					<?php
					
				} ?>
        </div>

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom ">
            <h1 class="h2 <?= !($_REQUEST[ 'idFormulario' ] ?? '') ? '' : 'd-none'; ?>">Subir CSV</h1>
        </div>

        <form action="proccess_csv.php"
              class="<?= ($_SESSION[ 'cod' ] ?? '') && !2 ?> "
              method="post" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h4>Cliente escogido <?= $_SESSION[ 'cod' ] ?? ''; ?> <?= $_SESSION['nombre_cliente'] ?? '';?></h4>
                    </div>
                    <div class="col-4">
                        <input type="file" placeholder="Subir fichero CSV" name="file" class="form-control"/>
                    </div>
                    <div class="col-2">
                        <button class="form-control btn btn-primary">
                            Subir
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <script>
            let urls = [];
        </script>
        <h4 class="<?= !($_SESSION[ 'cod' ] ?? '') ? '' : 'd-none'; ?> "> Debe escoger un cliente</h4>
        <div class="row ">
            <div class="col-12">
                <table class="table table-responsive">
                    <thead class="table-dark">
                    <tr>
                        <th>ID</th>
                        <th>Cliente</th>
                        <th>Nombre cliente</th>
                        <th>Fecha</th>
                        <th>Etiquetas</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
					<?php
						foreach (($lineas ?? []) as $linea): ?>
                            <tr>
                                <th><?= $linea[ 'idFormulario' ]; ?></th>
                                <th><?= $linea[ 'cliente' ]; ?></th>
                                <th><?= $linea[ 'destinatario' ]; ?></th>
                                <th><?= $linea[ 'fecha' ]; ?></th>
                                <th><a target="_BLANK" class="clickable"
                                       href="generar_etiquetas_from_upload.php?id=<?= $linea[ 'id' ]; ?>">IMPRIMIR</a></th>
                                <script>
                                    urls.push("generar_etiquetas_from_upload.php?id=<?= $linea[ 'id' ]; ?>")
                                </script>
                                <th><a href="javascript:;" onclick="eliminar_row(this, <?= $linea[ 'id' ]; ?>)">ELIMINAR</a></th>
                            </tr>
						<?php
						endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row <?= count($lineas ?? []) ? '': 'd-none';?>">
            <div class="col-12 text-center">
                <button class="btn btn-outline-warning w-25 d-none" onclick="imprimir_todas()" type="button">
                    Imprimir todas las etiquetas
                </button>
                <button class="btn btn-outline-success w-25" onclick="cerrar_formulario()">
                    Enviar Archivo
                </button>
            </div>
            <div class="col-12 text-center">
                Ruta del CSV generado <a href="" id="csv" target="_blank"></a>
            </div>
        </div>
    </main>
    <script>
        const imprimir_todas = _ => {
            i = 1;
            urls.forEach(enlace => setTimeout(_ => window.open(enlace), 300 * i));
        }
        const eliminar_row = (element, id) => {
            let respuesta = confirm(`¿Desea eliminar la línea con id ${id}?`)
            if (respuesta) {
                $.get(`delete_line.php?id=${id}`)
                    .done(function () {
                        $(element.closest('tr')).remove();
                    })
            }
        }
        const cerrar_formulario = _ => {
            let cliente = <?= $_SESSION[ 'cod' ] ?? '' ;?>;
            let idFormulario = <?= $_REQUEST['idFormulario'] ?? '';?>;
            let respuesta = confirm(`¿Desea cerrar este formulario?`)
            if (respuesta) {
                let consulta = $.get(`generate_csv_from_upload.php?cliente=${cliente}&idFormulario=${idFormulario}`)
                    .done(function () {
                        let csv = "http://" + location.hostname + "/clientes/ficheros/csv/" + consulta.responseText;
                        changeCSVURL(csv);
                    })
                    .fail(function () {
                    })
            }
            
        }
        const changeCSVURL = csv => {
            $("#csv").href = csv;
            $("#csv").text(csv);
        }
    </script>
<?php
	include("footer.php") ?>