<?php 
include("header.php");
$_SESSION['menu']=3;
if (isset($_REQUEST['codcli'])){
  $porciones = explode("-", $_REQUEST['codcli']);
  $_SESSION['cod']=$porciones[0];
}
if ($_SESSION['cod']!=""){ 
  if ($_SESSION['cod']=='1480' || $_SESSION['cod']=='1488'){
    $cod1='1480';
    $cod2='1488';
    $sqlex="SELECT * FROM inventari WHERE client=".$cod1." OR client=".$cod2." ORDER BY id ASC ";
    //$sqlex="SELECT * FROM inventari WHERE client=".$_SESSION['cod'];
  }else{
    $sqlex="SELECT * FROM inventari WHERE client=".$_SESSION['cod']." ORDER BY id ASC ";
  }  
  $consultaex=mysqli_query($conexion,$sqlex);
  $totalex=mysqli_num_rows($consultaex);
  
}


if($_SESSION['user']==""){
  header('Location:sign-in.php?&error=3');
}

?>

  <body>

    <?php include("main.php")?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
            <h1 class="h2">Almacen</h1>           
          </div>

          <?php if ($_SESSION['nivel']==1){
            $sqlclientes="SELECT * FROM codisclients";
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            ?>
             <div class="col-md-3 col-xs-12">
            <form method="post">
              <input list="codcli" class="form-control" name="codcli" type="text" placeholder="Elige un cliente"> 
           <datalist id="codcli">            
            <?php
             while($resultadoclientes=mysqli_fetch_assoc($consultaclientes)){?>
              <option value="<?php echo $resultadoclientes['codi'].'-'.$resultadoclientes['nom']?>">
            <?php
            }?>

          </datalist>       
          <input type="submit" value="Buscar" class="btn btn-primary btn-sm">
          </form>
        </div>
          

          <br>

          <?php
          if ($_SESSION['cod']>0){
            $sqlclientes="SELECT * FROM codisclients WHERE codi=".$_SESSION['cod'];
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            $resultadoclientes=mysqli_fetch_assoc($consultaclientes);
            ?>
      		<h4>Cliente <?php echo $_SESSION['cod'].' '.$resultadoclientes['nom']?></h4><br><br>

          <?php 
          }
          } ?>
          
          <div class="table-responsive">
            <?php if ($totalex>0){?>
            <table class="table table-striped table-sm" width="100%" id="example">
              <thead>
                <tr>        
                  <th>#</th>          
                  <th>Artículo</th>
                  <th>Nombre</th>
                  <th>t/u</th>
                  <th>Stock</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                while($resultadoex=mysqli_fetch_assoc($consultaex)){?>
                <tr>
                  <td><?php echo $resultadoex['id']?></td>
                  <td><?php echo $resultadoex['article']?></td>
                  <td><?php echo utf8_encode($resultadoex['nom'])?></td>
                  <td><?php echo $resultadoex['tu']?></td>
                  <td><?php echo $resultadoex['stock']?></td>
                  <td><a href="movimientos.php?&art=<?php echo $resultadoex['article']?>"><button class="btn btn-primary btn-sm">Ver</button></a></td>
                </tr>      
                <?php
                }?>          
              </tbody>
              <tfoot>
                <tr>        
                  <th>#</th>          
                  <th>Artículo</th>
                  <th>Nombre</th>
                  <th>t/u</th>
                  <th>Stock</th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          <?php 
        }?>
       
          </div>
        </main>
         
       
<?php include("footer.php")?>

<script>
    $(document).ready(function() {
        $('#example').dataTable( {
            "stateSave": false,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "order": [[ 1, "asc" ]],
            "language": {
                "lengthMenu": "Registros a mostrar _MENU_ por página",
                "zeroRecords": "No he encontrado nada lo siento",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin registros disponibles",
                "infoFiltered": "(filtro de _MAX_ registros)",
                "sSearch": "Buscar",
                "oPaginate": {
                    'sFirst':    'Primero',
                    'sPrevious': 'Anterior',
                    'sNext':     'Siguiente',
                    'sLast':     'Último'
                    }
                }
            } ); 
        });
    </script> 