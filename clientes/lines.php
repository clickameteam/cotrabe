<?php
    include("conn.php");
    
    $idFormulario = $_REQUEST['idFormulario'] ?? 0;
    $data = [':idFormulario' => $idFormulario];
    $sql = "SELECT * FROM lineas where idFormulario = :idFormulario AND status <> 9";
    $result = $mdb->prepare($sql);
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    echo $result;

