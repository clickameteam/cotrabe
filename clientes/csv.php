<?php 
include("header.php");
$_SESSION['menu']=5;
if (isset($_REQUEST['codcli'])){
  $porciones = explode("-", $_REQUEST['codcli']);
  $_SESSION['cod']=$porciones[0];
}
if ($_SESSION['cod']!=""){ 
  if ($_SESSION['cod']=='1480' || $_SESSION['cod']=='1488'){
    $cod1='1480';
    $cod2='1488';
    $sqlex="SELECT * FROM inventari WHERE client=".$cod1." OR client=".$cod2." ORDER BY id ASC ";
    //$sqlex="SELECT * FROM inventari WHERE client=".$_SESSION['cod'];
  }else{
    $sqlex="SELECT * FROM inventari WHERE client=".$_SESSION['cod']." ORDER BY id ASC ";
  }  
  $consultaex=mysqli_query($conexion,$sqlex);
  $totalex=mysqli_num_rows($consultaex);
  
}


if($_SESSION['user']==""){
  header('Location:sign-in.php?&error=3');
}

?>

  <body>

    <?php include("main.php")?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
            <h1 class="h2">Formularios</h1>           
          </div>
          <div class="row">
            <div class="col-12">
              <table class="table table-responsive">
                <thead class="table-dark">
                  <tr>
                    <th>Eliminar</th>
                    <th>Albarán</th>
                    <th>Fecha</th>
                    <th>Cliente</th>
                    <th>Dirección</th>
                    <th>CP</th>
                    <th>Población</th>
                    <th>Teléfono</th>
                    <th>Horario</th>
                    <th>Bultos</th>
                    <th>Kilos</th>
                    <th>Fecha aplazada</th>
                    <th>Observaciones</th>
                    <th>Reembolso</th>
                    <th>Portes</th>
                    <th>Artículo</th>
                    <th>Unidades</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                  </tr>
                </thead>
                <tbody id="tbody">
                  
                </tbody>
              </table>
            </div>
          </div>
          <?php if ($_SESSION['nivel']==1){
            $sqlclientes="SELECT * FROM codisclients";
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            ?>
             <div class="col-md-12 col-xs-12">
            <form method="post">
              <input list="codcli" style="width:70%; height:32px;" name="codcli" type="text" placeholder="Elige un cliente"> 
              <datalist id="codcli">            
                <?php
                while($resultadoclientes=mysqli_fetch_assoc($consultaclientes)){?>
                  <option value="<?php echo $resultadoclientes['codi'].'-'.$resultadoclientes['nom']?>">
                <?php
                }?>

              </datalist>       
              <input type="submit" value="Buscar" class="btn btn-primary btn-sm">
          </form>
        </div>
          

          <br>

          <?php
          if ($_SESSION['cod']>0){
            $sqlclientes="SELECT * FROM codisclients WHERE codi=".$_SESSION['cod'];
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            $resultadoclientes=mysqli_fetch_assoc($consultaclientes);
            ?>
      		<h4>Cliente <?php echo $_SESSION['cod'].' '.$resultadoclientes['nom']?></h4><br><br>

          <?php 
          }
          } ?>
          
          <div class="table-responsive">
            <?php if ($totalex>0){?>
              
              <?php 
            }?>
          
          </div>
          <form action="">
            <input 
            type="number" 
            value="<?= $_SESSION['cod'];?>" 
            disabled placeholder="Código cliente cotrabe" class="form-control" name="cliente" max=6>
            <input 
            type="number" 
            placeholder="Su número de albarán" pattern="[A-Za-z0-9]+" class="form-control" name="albaran" max=12>
            <input 
            type="number" 
            placeholder="Fecha del pedido AAAAMMDD" class="form-control" name="fecha" max=8>
            <input 
            type="text" 
            placeholder="Nombre comercial del destinatario" 
            pattern="[A-Za-z0-9]+" class="form-control" name="destinatario" max=35>
            <input 
            type="text" 
            placeholder="Dirección de envío" pattern="[A-Za-z0-9]+" class="form-control" name="direccion" max=35>
            <input 
            type="number" 
            placeholder="Código postal del envío" class="form-control" name="cp" max=5>
            <input 
            type="text" 
            placeholder="Población del envío" pattern="[A-Za-z0-9]+" class="form-control" name="poblacion" max=35>
            <input 
            type="text" 
            placeholder="Teléfono del cliente" pattern="[A-Za-z0-9]+" class="form-control" name="telefono" max=10>
            <input 
            type="tect" 
            placeholder="Horario de entrega" pattern="[A-Za-z0-9]+" class="form-control" name="horario" max=25>
            <input 
            type="number" 
            placeholder="Número de bultos del envío. Únicamente si sólo efectuamos el transporte" 
            class="form-control" name="bultos" max=15>
            <input 
            type="number" 
            placeholder="Num de kilos del envío. Únicamente si sólo efectuamos el transporte" 
            class="form-control" name="kilos" max=10>
            <input 
            type="number" 
            placeholder="Fecha de entrega aplazada (AAAAMMDD)" class="form-control" name="aplazada" max=10>
            <input 
            type="text" 
            placeholder="Comentarios para la entrega" 
            pattern="[A-Za-z0-9]+" class="form-control" name="observaciones" max=48>
            <input 
            type="number" 
            placeholder="Importe a cobrar a la descarga (999999.99)" class="form-control" name="reembolso" max=8>
            <input 
            type="text" 
            placeholder="Tipo portes siempre pagados por el remitente cliente" pattern="[A-Za-z0-9]+" class="form-control" name="portes" max=7>
            <input 
            type="text" 
            placeholder="Su código de producto" pattern="[A-Za-z0-9]+" class="form-control" name="articulo" max=15>
            <input 
            type="number" 
            placeholder="Unidades del producto" class="form-control" name="unidades" max=6>
            
            <button type="button" class="btn btn-primary" onclick="insert_line(this);">
              Añadir otra línea
            </button>
            <button type="button" class="btn btn-success" onclick="cerrar_formulario()">
              Finalizar
            </button>
          </form>
        </main>
         
       
<?php include("footer.php")?>

<script>
  $(document).ready(function() {
      $('#example').dataTable( {
          "stateSave": false,
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
          "order": [[ 1, "asc" ]],
          "language": {
              "lengthMenu": "Registros a mostrar _MENU_ por página",
              "zeroRecords": "No he encontrado nada lo siento",
              "info": "Mostrando página _PAGE_ de _PAGES_",
              "infoEmpty": "Sin registros disponibles",
              "infoFiltered": "(filtro de _MAX_ registros)",
              "sSearch": "Buscar",
              "oPaginate": {
                  'sFirst':    'Primero',
                  'sPrevious': 'Anterior',
                  'sNext':     'Siguiente',
                  'sLast':     'Último'
                  }
              }
          } ); 
      });
    
</script> 
<script>
  const insert_line = element => {
    let inputs = element.closest('form').querySelectorAll('input');
    let elements = {};
    inputs.forEach(e => elements[e.name] = e.value);
    let consulta = $.post("add_line.php", elements)
    .done(function(){
      let tr = JSON.parse(consulta.responseText)[0];
      insert_row(tr);
    })
  }
  const insert_row = tr => {
    $("#tbody").append(`
    <tr>
      <td><a href="javascript:;" onclick="eliminar_row(this, ${tr.id})">Eliminar ${tr.id}</a></td>
      <td>${tr.albaran}</td>
      <td>${tr.fecha}</td>
      <td>${tr.destinatario}</td>
      <td>${tr.direccion}</td>
      <td>${tr.cp}</td>
      <td>${tr.poblacion}</td>
      <td>${tr.telefono}</td>
      <td>${tr.horario}</td>
      <td>${tr.bultos}</td>
      <td>${tr.kilos}</td>
      <td>${tr.aplazada}</td>
      <td>${tr.observaciones}</td>
      <td>${tr.reembolso}</td>
      <td>${tr.portes}</td>
      <td>${tr.articulo}</td>
      <td>${tr.unidades}</td>
      <td>${tr.status}</td>
      <td>${tr.created_at}</td>
      <td>${tr.updated_at}</td>
    </tr>
    `);
  }
  const eliminar_row = (element, id) => {
    let respuesta = confirm(`¿Desea eliminar la línea con id ${id}?`)
    if (respuesta){
      $.get(`delete_line.php?id=${id}`)
      .done(function(){
        $(element.closest('tr')).remove();
      })
    }
    else{
      
    }
  }
  const init_table = _ => {
    let cliente = document.querySelector("input[name='cliente']").value;
    let consulta = $.get(`lines.php?cliente=${cliente}`)
    .done(function(){
      let trs = JSON.parse(consulta.responseText);
      $.each(trs, function(){
        insert_row(this);
      })
    })
  }
  const cerrar_formulario = _ => {
    let cliente = document.querySelector("input[name='cliente']").value;
    let respuesta = confirm(`¿Desea cerrar este formulario y generar el CSV?`)
    if (respuesta){
      $.get(`generate_csv.php?cliente=${cliente}`)
      .done(function(){
        alert("CSV generado");
        $("#tbody").empty();
      })
    }
    else{
      
    }
  }
  $(document).ready(_ => {
    init_table();
  })
</script>