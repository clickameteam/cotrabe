<!-- Ventana Editar Registros CRUD -->
<div class="modal fade" id="edit_<?php echo $resultadoex['dest_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
				<h4 class="modal-title" id="myModalLabel">Editar Destinatario</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>   
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="destinatarios_operaciones.php?id=<?php echo $resultadoex['dest_id']; ?>">
				<div class="row form-group">
					<div class="col-sm-2">
						<label class="control-label" style="position:relative; top:7px;">Nombre Comercial:</label>
					</div>
					<div class="col-sm-10">
						<input type="text"  class="form-control" max="35" name="destinatario" value="<?php echo $resultadoex['dest_name']; ?>" required>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-2">
						<label class="control-label" style="position:relative; top:7px;">Direccion:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control"  max="35" name="direccion" value="<?php echo $resultadoex['dest_direccion']; ?>" required>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-2">
						<label class="control-label" style="position:relative; top:7px;">Codigo Postal:</label>
					</div>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="cp" maxlength="5" value="<?php echo $resultadoex['dest_codigop']; ?>" required>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-2">
						<label class="control-label" style="position:relative; top:7px;">Poblacion:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control"  max="35" required name="poblacion" value="<?php echo $resultadoex['dest_poblacion']; ?>">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-sm-2">
						<label class="control-label" style="position:relative; top:7px;">Telefono:</label>
					</div>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="telefono" max="10" value="<?php echo $resultadoex['dest_telefono']; ?>" required>
					</div>
					<input type="hidden" name="edition">
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fas fa-times"></span> Cancel</button>
                <button type="submit" name="editar" class="btn btn-success"><span class="fas fa-check"></span> Actualizar Ahora</button>
			</form>
            </div>

        </div>
    </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete_<?php echo $resultadoex['dest_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Borrar Destinatario</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
            <div class="modal-body">	
            	<p class="text-center">¿Esta seguro de Borrar el registro?</p>
				<h2 class="text-center"><?php echo $resultadoex['dest_name']; ?></h2>
				<h3 class="text-center"><?php echo $resultadoex['dest_direccion']; ?></h3>
			</div>
			<div class="modal-footer">
				<form method="POST" action="destinatarios_operaciones.php?id=<?php echo $resultadoex['dest_id']; ?>">
					<input type="hidden" name="deletion">
					<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fas fa-times"></span> Cancelar</button>
                	<button type="submit" name="borrar" class="btn btn-danger"><span class="fas fa-trash"></span> Si</button>
				</form>
			</div>

        </div>
    </div>
</div>