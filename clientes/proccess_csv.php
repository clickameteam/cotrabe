<?php
	error_reporting(E_ALL);
	error_reporting(-1);
	ini_set('error_reporting', E_ALL);
	ini_set('auto_detect_line_endings', TRUE);
	session_start();
	
	include("conn.php");
	echo $_FILES['file']['tmp_name'];
	$csv_file = fopen($_FILES['file']['tmp_name'], 'r');
	$idCliente = $_SESSION[ 'cod' ];
	
	$cliente = "SELECT * FROM codisclients WHERE codi='$_SESSION[cod]'";
	$result = $mdb->prepare($cliente);
	$result->execute();
	$cliente = $result->fetch(PDO::FETCH_ASSOC)[ 'nom' ];
	$fecha = date('Y-m-d h:i:s');
	$idFormulario = $_REQUEST[ 'idFormulario' ] ?? 0;
	$data = [ ':cliente' => $_SESSION[ 'cod' ], ':fecha' => $fecha ];
	$sql = "INSERT INTO ficheros(cliente, created_at, updated_at, status) VALUES (:cliente, :fecha, :fecha, 1)";
	$result = $mdb->prepare($sql);
	$result->execute($data);
	$idFormulario = $mdb->lastInsertId();
	
	$sql = "INSERT INTO `envios` (`id`) VALUES (NULL)";
	$result = $mdb->prepare($sql);
	$result->execute();
	$idEnvio = $mdb->lastInsertId();
	
	while (($datos = fgetcsv($csv_file, 10000, ';')) !== FALSE) {
		$i = 0;
		$value = [];
		$value['idEnvio'] = $idEnvio;
		$value[ 'idFormulario' ] = $idFormulario;
		$value[ 'cliente' ] = $_SESSION[ 'cod' ];
		$i++;
		$value[ 'albaran' ] = $datos[ $i++ ];
		$value[ 'fecha' ] = $datos[ $i++ ];
		$value[ 'destinatario' ] = $datos[ $i++ ];
		$value[ 'direccion' ] = $datos[ $i++ ];
		$value[ 'cp' ] = $datos[ $i++ ];
		$value[ 'poblacion' ] = $datos[ $i++ ];
		$value[ 'telefono' ] = $datos[ $i++ ];
		$value[ 'horario' ] = $datos[ $i++ ];
		$value[ 'bultos' ] = $datos[ $i++ ];
		$value[ 'kilos' ] = $datos[ $i++ ];
		$value[ 'aplazada' ] = $datos[ $i++ ];
		$value[ 'observaciones' ] = $datos[ $i++ ];
		$value[ 'reembolso' ] = $datos[ $i++ ];
		$value[ 'portes' ] = $datos[ $i++ ];
		$value[ 'articulo' ] = $datos[ $i++ ];
		$value[ 'unidades' ] = $datos[ $i++ ];
		$value[ 'idEnvio' ] = $datos[ $i++ ];
		$sql = "
            INSERT INTO `lineas`(`id`, `idFormulario`, `idEnvio`, `cliente`,
            `albaran`, `fecha`, `destinatario`, `direccion`, `cp`,
            `poblacion`, `telefono`, `horario`, `bultos`, `kilos`, `aplazada`,
            `observaciones`, `reembolso`, `portes`, `articulo`, `unidades`, `status`,
            `created_at`, `updated_at`)
            VALUES (0, :idFormulario, :idEnvio, :cliente, :albaran, :fecha,:destinatario,
            :direccion,:cp,:poblacion,:telefono,:horario, :bultos,:kilos,:aplazada,:observaciones,
            :reembolso, :portes,:articulo, :unidades, 1,
            'NOW()', 'NOW()' )";
		$result = $mdb->prepare($sql);
		$result->execute($value);
		
		
	}
	ini_set('auto_detect_line_endings', FALSE);
    
?>

<script>
	location.href="subir_csv.php?idFormulario=<?= $idFormulario;?>";
</script>
