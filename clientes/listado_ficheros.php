<?php
	include("header.php");
	$_SESSION['menu'] = 6;
	if ($_SESSION["nivel"] == 1){
		$sql              =
			"SELECT *, ficheros.id as idFichero FROM ficheros INNER JOIN codisclients ON codisclients.codi = ficheros.cliente ORDER BY ficheros.id DESC";
	}
	else{
		$sql              =
			"SELECT *, ficheros.id as idFichero FROM ficheros INNER JOIN codisclients ON codisclients.codi = ficheros.cliente WHERE cliente = '$_SESSION[cod]' ORDER BY ficheros.id DESC";
	}
	$query            = mysqli_query($conexion, $sql);
	$ficheros         = mysqli_fetch_all($query, MYSQLI_ASSOC);
	
	if ($_SESSION['user'] == "") {
		header('Location:sign-in.php?&error=3');
	}
	
?>

<body>

<?php include("main.php") ?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<input type="hidden" id="codigoCliente" value="<?= $_SESSION['cod']; ?>"/>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
		<h1 class="h2">Formularios</h1>
	</div>
	<div class="row">
		<?php if ($_SESSION['nivel']==1){
			$sqlclientes="SELECT * FROM codisclients";
			$consultaclientes=mysqli_query($conexion,$sqlclientes);
			?>
			<form method="post" action="generar_expediciones.php">
				<div class="col-12">
					<div class="row">
						
						<div class="col-md-6 col-xs-12">
							<input list="codcli" class="form-control" name="codcli" type="text" placeholder="Elige un cliente" required>
							<datalist id="codcli">
								<?php
									while($resultadoclientes=mysqli_fetch_assoc($consultaclientes)){?>
								<option value="<?php echo $resultadoclientes['codi'].'-'.$resultadoclientes['nom']?>">
									<?php
										}?>
							
							</datalist>
						</div>
						<div class="col-md-6 col-xs-12">
							<input type="submit" class="btn btn-primary btn-sm form-control" value="Crear nuevo formulario">
						</div>
					</div>
				</div>
			</form>
			<br>
			<?php
			
		} ?>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="table table-responsive" id="example2">
				<thead class="table-dark">
				<tr>
					<th>ID</th>
					<th>Cliente</th>
					<th>Nombre cliente</th>
					<th>Fecha</th>
					<th>Status</th>
					<th>Acción</th>
					<th>CSV</th>
					<th>PDF</th>
				</tr>
				</thead>
				<tbody id="tbody">
				<?php
					foreach ($ficheros as $fichero) {
						$csv    = $fichero['csv'] ? "<a href='ficheros/csv/$fichero[csv]'>CSV</a>" : "";
						$pdf    = $fichero['pdf'] ? "<a href='ficheros/pdf/$fichero[pdf]'>PDF</a>" : "";
						$status = $fichero['status'] ? 'Finalizado' : 'Abierto';
						$accion = $fichero['status'] ? 'Ver' : 'Editar';
						$date   = date('d/m/Y H:i', strtotime($fichero['created_at']));
						echo "<tr>
	                            <td>$fichero[idFichero]</td>
	                            <td>$fichero[cliente]</td>
	                            <td>$fichero[nom]</td>
	                            <td>$date</td>
	                            <td>$status</td>
	                            <td><a href='generar_expediciones.php?idFormulario=$fichero[idFichero]&verFormulario=1'>$accion</a></td>
	                            <td>$csv</td>
	                            <td>$pdf</td>
	                        </tr>";
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
	<?php if ($_SESSION['nivel'] == 1) {
		$sqlclientes      = "SELECT * FROM codisclients";
		$consultaclientes = mysqli_query($conexion, $sqlclientes);
		?>
		<div class="col-md-12 col-xs-12">
		
		</div>
		
		
		<br>
		
		<?php
		if ($_SESSION['cod'] > 0) {
			$sqlclientes       = "SELECT * FROM codisclients WHERE codi=" . $_SESSION['cod'];
			$consultaclientes  = mysqli_query($conexion, $sqlclientes);
			$resultadoclientes = mysqli_fetch_assoc($consultaclientes);
			?>
			<h4>Cliente <?php echo $_SESSION['cod'] . ' ' . $resultadoclientes['nom'] ?></h4><br><br>
			
			<?php
		}
	} ?>
	
	<div class="table-responsive">
		<?php if ($totalex ?? '' > 0) { ?>
			
			<?php
		} ?>
	</div>

</main>

<div id="resultado"></div>
<?php include("footer.php") ?>
<style>
	table{
		 width: 100vw;
	}
	table td {
		word-break: break-all !important;
	}
</style>
<script>
	$(document).ready(function () {
		$('#example2').dataTable({
			"stateSave": false,
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
			"order": [[0, "desc"]],
			"language": {
				"lengthMenu": "Registros a mostrar _MENU_ por página",
				"zeroRecords": "No he encontrado nada lo siento",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"infoEmpty": "Sin registros disponibles",
				"infoFiltered": "(filtro de _MAX_ registros)",
				"sSearch": "Buscar",
				"oPaginate": {
					'sFirst': 'Primero',
					'sPrevious': 'Anterior',
					'sNext': 'Siguiente',
					'sLast': 'Último'
				}
			}
		});
	});
</script>
