
<?php
include("header.php");
$_SESSION['menu']=2;
if (isset($_REQUEST['codcli'])){
  $porciones = explode("-", $_REQUEST['codcli']);
  $_SESSION['cod']=$porciones[0];
}
if (isset($_SESSION['cod'])){
   if ($_SESSION['cod']=='1480' || $_SESSION['cod']=='1488'){
    $cod1='1480';
    $cod2='1488';
    if (isset($_REQUEST['fecha1'])){
      $sqlex="SELECT * FROM expedicions WHERE (cliente=".$cod1." OR  cliente=".$cod2.") AND fechaorden BETWEEN '".$_REQUEST['fecha1']."' and '".$_REQUEST['fecha2']."' ORDER BY fechaorden DESC";
     // $sqlex="SELECT * FROM expedicions WHERE cliente=".$_SESSION['cod']." AND fechaorden BETWEEN '".$_REQUEST['fecha1']."' and '".$_REQUEST['fecha2']."' ORDER BY expedicion ASC";
    }else{
      $sqlex="SELECT * FROM expedicions WHERE cliente=".$cod1." OR  cliente=".$cod2." AND fechaorden >= date_sub(curdate(), interval 2 month) ORDER BY fechaorden DESC";
      //$sqlex="SELECT * FROM expedicions WHERE cliente=".$_SESSION['cod']." ORDER BY expedicion ASC";
    }
   }else{
    if (isset($_REQUEST['fecha1'])){
      $sqlex="SELECT * FROM expedicions WHERE cliente=".$_SESSION['cod']." AND fechaorden BETWEEN '".$_REQUEST['fecha1']."' and '".$_REQUEST['fecha2']."' ORDER BY fechaorden DESC";
    }else{
      $sqlex="SELECT * FROM expedicions WHERE cliente=".$_SESSION['cod']." AND fechaorden >= date_sub(curdate(), interval 1 month) ORDER BY fechaorden DESC";
    }
   }
  
  $consultaex=mysqli_query($conexion,$sqlex);
  $totalex=mysqli_num_rows($consultaex);
}

if($_SESSION['user']==""){
  header('Location:sign-in.php?&error=3');
}

?>
<script type="text/javascript">
function crit_busqueda() {
  var input=document.getElementById('texto_busqueda').value.toLowerCase();
  var output=document.getElementById('codcli').options;
  for(var i=0;i<output.length;i++) {
    if(output[i].value.indexOf(input)==0){
      output[i].selected=true;
      }
    if(document.forms[0].texto_busqueda.value==''){
      output[0].selected=true;
      }
  }
}
</script>
  <body>

    <?php include("main.php")?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
            <h1 class="h2">Expediciones</h1>
           
          </div>
          <?php if ($_SESSION['nivel']==1){
            $sqlclientes="SELECT * FROM codisclients";
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            ?>
             <div class="col-md-3 col-xs-12">
            <form method="post">
            <input list="codcli" class="form-control" name="codcli" type="text" placeholder="Elige un cliente">
            <datalist id="codcli">
            <?php
             while($resultadoclientes=mysqli_fetch_assoc($consultaclientes)){?>
              <option value="<?php echo $resultadoclientes['codi'].'-'.$resultadoclientes['nom']?>">
            <?php
            }?>

          </datalist>

          <input type="submit" class="btn btn-primary btn-sm" value="Buscar">
          </form>
      	</div>
          <br>
          <?php
          if ($_SESSION['cod']!=""){
            $sqlclientes="SELECT * FROM codisclients WHERE codi=".$_SESSION['cod'];
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            $resultadoclientes=mysqli_fetch_assoc($consultaclientes);
            ?>
          <h4>Cliente <?php echo ($_SESSION['cod'] ?? 'Debe seleccionar un cliente').' '.($resultadoclientes['nom'] ?? '');?></h4><br><br>

          <?php
          }
          } ?>
          
          <form>
          Desde:<input type="date" name="fecha1" id="fecha1"> Hasta:<input type="date" name="fecha2" id="fecha2"> <input type="submit" class="btn btn-primary btn-sm" value="Buscar">
          </form>
         
          <div class="table-responsive">

                <table class="table table-striped table-sm" width="100%" id="example3">
              <thead>
                <tr>
                  <th width="5%">Expedición</th>
                  <th width="5%">Fecha</th>
                  <th width="10%">Estado</th>
                  <th width="15%">Albarán</th>
                  <th width="10%">Horario</th>
                  <th width="15%">Cliente</th>
                  <th width="10%">Destino</th>
                  <th width="10%">Domicilio</th>
                  <th width="10%">CP</th>
                  <th width="10%">Población</th>
                </tr>
              </thead>
              <tbody>
            
            
                <?php
                if ($totalex>0){
                while($resultadoex=mysqli_fetch_assoc($consultaex)){?>
              


                <?php
                  $estado="";
                  if ($resultadoex['adys']==0){
                    $estado="PENDIENTE";
                  }else if($resultadoex['adys']==1){
                    $estado="ASIGNADO";
                  }else if ($resultadoex['adys']==2){
                    $estado="EN TRANSITO";
                  }else if ($resultadoex['adys']==3){
                    $estado="ENTREGADO";
                  }else if ($resultadoex['adys']==4){
                    $estado="INCIDENCIA";
                  }else if ($resultadoex['adys']==5){
                    $estado="TRAMITADO";
                  }else if ($resultadoex['adys']==6){
                    $estado="DEV. ORIGEN";
                  }else if ($resultadoex['adys']==7){
                    $estado="ANULADA";
                  }else if ($resultadoex['adys']==8){
                    $estado="ENTREGADO APP";
                  } else {
                    $estado = $resultadoex['adys'];
                  }
                  $estado .= " - ".$resultadoex['adys'];
                  $mydate = strtotime($resultadoex['fecha']);
                  $newformat = date('Y-m-d',$mydate);
                  ?>
                  <tr>
                    <td><b><?php echo $resultadoex['expedicion'] ?></b></td>
                    <td><?php echo $resultadoex['fecha'] ?></td>
                    <td><?php echo $estado ?></td>
                    <td><?php echo $resultadoex['observaciones'] ?></td>
                    <td><?php echo $resultadoex['horario'] ?></td>
                    <td><?php echo $resultadoex['nomcliente'] ?></td>
                    <td><?php echo $resultadoex['nomdestino'] ?></td>
                    <td><?php echo utf8_decode($resultadoex['domicilio']) ?></td>
                    <td><?php echo $resultadoex['cp'] ?></td>
                    <td><?php echo $resultadoex['poblacion'] ?></td>
                  </tr>
                  <?php
                  $sqlmov="SELECT * FROM moviments WHERE expedicion LIKE '%".intval($resultadoex['expedicion'])."%'";
                  $consultamov=mysqli_query($conexion,$sqlmov);
                  $totalmov=mysqli_num_rows($consultamov);
                  ?>
            <?php
            }
            }?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Expedición</th>
                  <th>Fecha</th>
                  <th>Estado</th>
                  <th>Albarán</th>
                  <th>Horario</th>
                  <th>Cliente</th>
                  <th>Destino</th>
                  <th>Domicilio</th>
                  <th>CP</th>
                  <th>Población</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </main>
<?php include("footer.php")?>
<script>
    $(document).ready(function() {
        $('#example3').dataTable( {
            "stateSave": false,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "ordering" : true,
            "columnDefs" : [{"targets":1, "type":"date-eu"}],
            "order": [1, 'desc'],
            "language": {
                "lengthMenu": "Registros a mostrar _MENU_ por página",
                "zeroRecords": "No he encontrado nada lo siento",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin registros disponibles",
                "infoFiltered": "(filtro de _MAX_ registros)",
                "sSearch": "Buscar",
                "oPaginate": {
                    'sFirst':    'Primero',
                    'sPrevious': 'Anterior',
                    'sNext':     'Siguiente',
                    'sLast':     'Último'
                    }
                }
            } );
        });
    </script>