<?php 
include("header.php");
$_SESSION['menu']=7;
if (isset($_REQUEST['codcli'])){
  $porciones = explode("-", $_REQUEST['codcli']);
  $_SESSION['cod']=$porciones[0];
}
if ($_SESSION['cod']!=""){ 
  if ($_SESSION['cod']=='1480' || $_SESSION['cod']=='1488'){
    $cod1='1480';
    $cod2='1488';
    $sqlex="SELECT * FROM destinatarios WHERE usuario_id=".$cod1." OR usuario_id=".$cod2." ORDER BY dest_id ASC ";
    //$sqlex="SELECT * FROM destinatarios WHERE usuario_id=".$_SESSION['cod'];
  }else{
    $sqlex="SELECT * FROM destinatarios WHERE usuario_id=".$_SESSION['cod']." ORDER BY dest_id ASC ";
  }  
  $consultaex=mysqli_query($conexion,$sqlex);
   $totalex=mysqli_num_rows($consultaex);
   
}


if($_SESSION['user']==""){
  header('Location:sign-in.php?&error=3');
}

?>

  <body>

    <?php include("main.php")?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
            <h1 class="h2">Listado de Destinatarios</h1>           
          </div>
          
          <?php if(isset($_SESSION['message'])){ ?>
		
		      <div class="alert alert-info text-center" style="margin-top:20px;">
          <?php echo $_SESSION['message']; ?>
          </div>
		      <?php 
		unset($_SESSION['message']);
	}?>

          <?php if ($_SESSION['nivel']==1){
            $sqlclientes="SELECT * FROM codisclients";
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            ?>
             <div class="col-md-3 col-xs-12">
            <form method="post">
              <input list="codcli" class="form-control" name="codcli" type="text" placeholder="Elige un cliente"> 
           <datalist id="codcli">            
            <?php
             while($resultadoclientes=mysqli_fetch_assoc($consultaclientes)){?>
              <option value="<?php echo $resultadoclientes['codi'].'-'.$resultadoclientes['nom']?>">
            <?php
            }?>

          </datalist>       
          <input type="submit" value="Buscar" class="btn btn-primary btn-sm">
          </form>
        </div>
          
          <br>

          <?php
          if ($_SESSION['cod']>0){
            $sqlclientes="SELECT * FROM codisclients WHERE codi=".$_SESSION['cod'];
            $consultaclientes=mysqli_query($conexion,$sqlclientes);
            $resultadoclientes=mysqli_fetch_assoc($consultaclientes);
            ?>
      		<h4>Cliente <?php echo $_SESSION['cod'].' '.$resultadoclientes['nom']?></h4><br><br>

          <?php 
          }
          } ?>
          
          <div class="col-sm-8 col-sm-offset-2">
			        <a href="#addnew" class="btn btn-primary" data-toggle="modal"><span class="fas fa-plus"></span> Nuevo Registro</a>
          </div>
          <br>
          <div class="table-responsive">
            <?php if ($totalex>0){?>
            <table class="table table-striped table-sm" width="100%" id="example">
              <thead>
                <tr>        
                  <th>#</th>          
                  <th>Nombre Comercial</th>
                  <th>Direccion</th>
                  <th>Codigo Postal</th>
                  <th>Población</th>
                  <th>Telefono</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                while($resultadoex=mysqli_fetch_assoc($consultaex)){?>
                <tr>
                  <td><?php echo $resultadoex['dest_id']?></td>
                  <td><?php echo $resultadoex['dest_name']?></td>
                  <td><?php echo utf8_encode($resultadoex['dest_direccion'])?></td>
                  <td><?php echo $resultadoex['dest_codigop']?></td>
                  <td><?php echo $resultadoex['dest_poblacion']?></td>
                  <td><?php echo $resultadoex['dest_telefono']?></td>
                  <td>
                    <a href="#edit_<?php echo $resultadoex['dest_id']?>" class="btn btn-success btn-sm" data-toggle="modal">
                      <span class="fas fa-edit"></span>Editar</a> 
                    <a href="#delete_<?php echo $resultadoex['dest_id']?>" class="btn btn-danger btn-sm" data-toggle="modal">
                      <span class="fas fa-trash"></span> Borrar</a>
                  </td>
                  
                </tr> 
                <?php include('destinatarios_modal.php'); ?>     
                <?php
                }?>          
              </tbody>
              <tfoot>
                <tr>        
                  <th>#</th>          
                  <th>Nombre Comercial</th>
                  <th>Direccion</th>
                  <th>Codigo Postal</th>
                  <th>Población</th>
                  <th>Telefono</th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          <?php 
        }?>
          <?php include('destinatarios_add_modal.php'); ?>
          </div>
        </main>
         
       
<?php include("footer.php")?>

<script>
    $(document).ready(function() {
        $('#example').dataTable( {
            "stateSave": false,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "order": [[ 1, "asc" ]],
            "language": {
                "lengthMenu": "Registros a mostrar _MENU_ por página",
                "zeroRecords": "No he encontrado nada lo siento",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Sin registros disponibles",
                "infoFiltered": "(filtro de _MAX_ registros)",
                "sSearch": "Buscar",
                "oPaginate": {
                    'sFirst':    'Primero',
                    'sPrevious': 'Anterior',
                    'sNext':     'Siguiente',
                    'sLast':     'Último'
                    }
                }
            } ); 
        });
    </script> 