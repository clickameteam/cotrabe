<?php
	session_start();
    include("conn.php");
    
    require_once("mpdf/vendor/autoload.php");
    require_once("clases/GenerateEtiquetas.php");
    require 'vendor/autoload.php';
    
    $idLinea = $_REQUEST['id'];
    $sql = "SELECT * FROM lineas WHERE id=:id";
    $result = $mdb->prepare($sql);
    $data = [':id' => $idLinea];    
    $result->execute($data);
    $result = $result->fetchAll(PDO::FETCH_OBJ);
    $result = json_encode($result);
    $result = json_decode($result, true);

    $nombrePDF = $idLinea.".pdf";
	$sql = "SELECT * FROM codisclients WHERE codi=:cliente";
	$data = [':cliente' => $result[0]['cliente']];
	$result2 = $mdb->prepare($sql);
	$result2->execute($data);
	$result2 = $result2->fetch(PDO::FETCH_ASSOC);
	$destinatario = $result2['nom'];
    $total = sizeof($result);
    if ($total == 0){
        header("HTTP/1.1 422 No hay órdenes que procesar");
        die('Error - No hay órdenes que procesar');
    }


    $etiquetas = new GenerateEtiquetas($nombrePDF, $result, $destinatario);
    $etiquetas->generate();


    echo "<script>location.href='ficheros/etiquetas/$nombrePDF';</script>";