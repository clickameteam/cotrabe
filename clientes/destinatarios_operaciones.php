<?php
	session_start();
	include_once('conn.php');

	$mdb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	$id = $_REQUEST ['id'];
	$user = $_SESSION['cod'];
	if(isset($_POST['edition'])){
		try{
			$datos = [
				":destinatario" => $_REQUEST['destinatario'] , 
				":direccion" => $_REQUEST['direccion'], 
				":cp" => $_REQUEST['cp'], 
				":poblacion" => $_REQUEST['poblacion'], 
				":telefono" => $_REQUEST['telefono']
			];
			$sql = "UPDATE destinatarios SET dest_name = :destinatario, dest_direccion = :direccion, dest_poblacion = :poblacion, dest_codigop = :cp, dest_telefono = :telefono WHERE dest_id = $id AND usuario_id = $user"; 
			
			
	
			$result = $mdb->prepare($sql);
			$_SESSION['message'] = ( $result->execute($datos) ) ? 'Destinatario actualizado correctamente' : 'No se pudo actualizar el destinatario';
			//if-else statement in executing our query
			//$_SESSION['message'] = ( $mdb->exec($sql) ) ? 'Empleado actualizado correctamente' : 'No se puso actualizar empleado';
			
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

	}
	else{
		$_SESSION['message'] = 'Complete el formulario de edición';
	}

	if(isset($_POST['deletion'])){
		
		try{
			$datos = [
				":id" => $id , 
				":user" => $user
			];
			$sql = "DELETE FROM destinatarios WHERE dest_id = $id AND usuario_id = $user";
			//if-else statement in executing our query
			$result = $mdb->prepare($sql);
			$_SESSION['message'] = ( $result->execute($datos) ) ? 'Destinatario Borrado' : 'Hubo un error al borrar destinatario';
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		

	}
	else{
		$_SESSION['message'] = 'Seleccionar Destinatario para eliminar primero';
	}

	if(isset($_POST['addition'])){
		try{
			$datos = [
				":destinatario" => $_REQUEST['destinatario'] , 
				":direccion" => $_REQUEST['direccion'], 
				":cp" => $_REQUEST['cp'], 
				":poblacion" => $_REQUEST['poblacion'], 
				":telefono" => $_REQUEST['telefono']
			];
			$sql = "INSERT INTO destinatarios (usuario_id, dest_name, dest_direccion, dest_poblacion, dest_codigop, dest_telefono) 
			VALUES ($user, :destinatario, :direccion, :poblacion, :cp, :telefono)"; 
			
			
	
			$result = $mdb->prepare($sql);
			$_SESSION['message'] = ( $result->execute($datos) ) ? 'Destinatario Agregado correctamente' : 'No se pudo agregar el destinatario';
			//if-else statement in executing our query
			
			
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

	}
	else{
		$_SESSION['message'] = 'Complete el formulario';
	}

	header('location: listado_destinatarios.php');

?>